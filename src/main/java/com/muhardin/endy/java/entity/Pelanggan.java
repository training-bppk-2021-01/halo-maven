package com.muhardin.endy.belajar.java.entity;

import java.time.LocalDate;

public class Pelanggan {
    private String nama;
    private String email;
    private LocalDate tanggalLahir;

    // getter
    public String getNama() {
        return nama;
    }

    // setter
    public void setNama(String x){
        nama = x;
    }
}